# stiymi
One line Golang install for Linux, Mac and Raspberry Pi


```bash
$ ./stiymi.sh 
Usage: bash goinstall.sh OPTION VERSION

OPTIONS:
  --32		Install 32-bit version
  --64		Install 64-bit version
  --darwin	Install on Mac
  --arm		Install armv6 version
  --remove	To remove currently installed version
```

Inspired by https://github.com/canha/golang-tools-install-script
